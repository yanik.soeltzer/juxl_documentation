
.. _X-Juxl-documentation:

Juxl documentation
==================

:strong:`Ju` pyter Lab :strong:`x` Api :strong:`L` ogging Interface documentation.

This documentation contains everything related to Juxl.
It is structured in two parts:

The first part :ref:`X-getting-started` providing an overview about Juxl and its components.


The second part describes each component of Juxl in more detail.







.. _X-getting-started:

###############
Getting Started
###############


This section provides an overview about Juxl.
It list the different components of Juxl and provides a short summary of each component.
Afterwards in :ref:`getting-started-use-cases`, use-cases of the Juxl application are reviewed and requirements are evaluated.


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Getting Started
   
   getting_started/what_is_juxl
   getting_started/components
   getting_started/use_cases
   getting_started/settings_composition


.. _X-juxl:

####
Juxl
####
  
.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Juxl
   
   Overview <juxl/index>
   juxl/installation
   juxl/configuration
   Deployment <juxl/deploy>
   juxl/setting_up_a_development_environment


.. _X-juxl-hub-auth:

#############
Juxl Hub Auth
#############


.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Juxl Hub Auth

   Overview <juxl_hub_auth/index>
   juxl_hub_auth/procedure
   juxl_hub_auth/installation
   juxl_hub_auth/configuration
   juxl_hub_auth/develop


