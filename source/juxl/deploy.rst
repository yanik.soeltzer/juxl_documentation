.. _juxl-deploy:

Deploy
======


We **highly** recommend using :ref:`juxl-hub-auth` to increase the security in a production environment.
See :ref:`Why should I use Juxl Hub Auth <juxl-hub-auth-why-juxl-hub-auth>` to understand how Juxl Hub Auth can increase the secuirty.



.. _juxl-deploy-using-docker:

Using Docker
************

If you want to deploy Juxl inside a docker image, you can use the :code:`jupyter labextension` cli to install the extensions.
This example installs the required 
:code:`@juxl/juxl-extension` and the juxl-extensions 
:code:`@juxl/logging`, 
:code:`@juxl/log-console` and
:code:`@juxl/performance-extension`.

For a complete docker image using juxl see `Juxl Docker Repository <https://git.rwth-aachen.de/learntech-lufgi9/bama/juxl/juxl_docker>`_.


.. code-block:: Dockerfile
	
	RUN jupyter labextension install \
	      @juxl/juxl-extension \
	      @juxl/logging \
	      @juxl/log-console \
	      @juxl/performance-extension



.. _juxl-deploy-default-settings:

Default Settings
****************

Changing the default settings fixes the problem of setting the settings for each user.

We recommend to use the :ref:`overrides.json <overrides.json>` to configure juxls default settings.
Please make sure you are familiar with the :ref:`settings composition <settings-composition>` of JupyterLab Extensions.

Most of the time, it is sensible to change the `agent` and `lrs` properties in order to log statements to an lrs.
For all available configurations see :ref:`Juxl Configuration <juxl-configuration>`.
This is an example overrides.json entry to set an pseudo-anonymous agent and configure an Juxl-Hub-Auth using a python-evnrionment variable:

.. code-block:: json
	
	{
	  "@juxl/juxl-extension:juxl": {
	    "agent": "pseudo-anonymous",
	    "lrs": {
	      "url": "http://my-juxl-hub-auth.com:3333",
	      "authentication": {
                "python-environment":"JUPYTERHUB_API_TOKEN"
	      }
	    }
	  }
	}
	
If you use the official :code:`jupyter/base-notebook:lab-3.1.6` notebook, the `overrides.json` can be copied into the docker container using:


.. code-block:: Dockerfile
	
	COPY --chown=1000 overrides.json /opt/conda/share/jupyter/lab/settings/overrides.json
