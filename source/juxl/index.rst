.. _juxl:

####
Juxl
####

Juxl is the heart of the Juxl environment.
It provides the Juxl Logging Interface.


**It does not log any statements by itselft.**


Instead it relies on Juxl-extensions such as :code:`@juxl/logging` to provide statements and Juxl-extensions such as :code:`@juxl/log-console` to visualize those statements.


.. note::

	
	The difference between :code:`@juxl/juxl` vs :code:`@juxl/juxl-extension`:

	:code:`@juxl/juxl` provides the Juxl Lunino token while :code:`@juxl/juxl-extension` provides a concrete implementation of the Lumino token.
	This concept decouples the actual implementation and interface and is a core concept of Jupyter Lab extensions.
	Since all Juxl-extensions only depende on the interface (:code:`@juxl/juxl`), the implementation (:code:`@juxl/juxl-extension`) can be substitued by another implementation
	You can read more about Jupyter Lab Tokens at the `offical documentation <https://jupyterlab.readthedocs.io/en/stable/extension/extension_dev.html#tokens>`_






.. note::
	
	Make sure To read the :ref:`getting-started-components` first to get an overview about how the different components work together.


