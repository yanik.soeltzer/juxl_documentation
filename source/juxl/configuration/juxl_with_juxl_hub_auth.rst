.. _juxl-configuration-juxl-with-juxl-hub-auth:

Configure Juxl with Juxl Hub Auth
=================================

We assume the following

* Multiple user use Jupyter Lab with Juxl
* Jupyter Lab is running within a Jupyter Hub context 
* Juxl Hub Auth is deployed at :code:`https://my.juxlHubAuth.com:3333`


For this setting, the recommended approach is to:

* configure Juxl using the :ref:`overrides.json <overrides.json>`
* use the :code:`JUPYTERHUB_API_TOKEN` environment variable for authentication
* use :code:`https://` connection



Using the :ref:`overrides.json <overrides.json>` ensures that Juxl is configured without interfering with the user settings.
This is especially relevant if for example the user home directoy is mounted by an external service.
By using the :code:`JUPYTERHUB_API_TOKEN` as the authentication method, we authenticate against Juxl Hub Auth using the :code:`JUPYTERHUB_API_TOKEN` of the current user.
See :ref:`juxl-hub-auth-why-juxl-hub-auth` for more information about security. 
Finally by explicitly using the :code:`https://` protocol to communicate with Juxl Hub Auth, we use a enrypted connection to tranmit xApi statements to Juxl Hub Auth. 


Content of `overrides.json`:

.. code-block:: json

	
	{
	  "@juxl/juxl-extension:juxl": {
	    "agent": "pseudo-anonymous",
	    "lrs": {
	      "url": "https://my.juxlHubAuth.com:3333",
	      "authentication": {
		"python-environment":"JUPYTERHUB_API_TOKEN"
	      }
	    }
	  }
	}

