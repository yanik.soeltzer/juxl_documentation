.. _juxl-config-all-options:

All Configuration Options
=========================

agent
*****


.. code-block::
	
	"agent": "pseudo-anonymous"


Configures the Jupyter Lab xApi Agent.
This agent is used to represent the current Jupyter Lab user in xApi statements.
For the statement `user opened notebook`, the user will be set to a pseudo-anonymous actor, resulting in `anonymous-2fxg opened notebook`.


There are three presets for the Jupyter Lab agent:


The pseudo-anonymous agent
^^^^^^^^^^^^^^^^^^^^^^^^^^ 

.. code-block::
	
	"agent": "pseudo-anonymous"
	
This agent setting  results in an psuedo-anonymous xApi actor.


.. code-block:: json
	
	{
	  "objectType": "Agent",
	  "account": {
	    "name": "anonymous-t8ozvwzpx",
	    "homePage": "http://accounts.pseudo-anonymous"
	  },
	  "name":"anonymous-t8ozvwzpx" 
	}


The pseudonym :code:`anonymous-t8ozvwzpx` is randomly generated and will be stored inside the :ref:`juxl-configuration-pseudonym` configuration variable.
This ensures that the same pseudonym will be used for the same user. 

The anonymous agent
^^^^^^^^^^^^^^^^^^^


.. code-block::
	
	"agent": "anonymous"

This agent setting results in an anonymous xApi actor.

.. code-block:: json
	
	{
	  "objectType": "Agent",
	  "account": {
	    "name": "Anonymous",
	    "homePage": "http://accounts.anonymous"
	  },
	  "name":"Anonymous" 
	}

The actor will be the same for all users that have set the agent to anonymous.
Therefore, later analysis can not distinguish between the different users.

The Jupyter Hub agent
^^^^^^^^^^^^^^^^^^^^^


.. code-block::
	
	"agent": "jupyterhub"
  

This setting is only available if Jupyter Lab is used with Jupyter Hub and results in an xApi actor generated from the JupyterHub actor.

.. code-block:: json
	
	{
	  "objectType": "Agent",
	  "account": {
	    "name": "ovjan",
	    "homePage": "http://jupyter.rwth-aachen.de/hub"
	  },
	  "name":"ovjan" 
	}

Where :code:`http://jupyter.rwth-aachen.de/hub` is the url of the Jupyter Hub instance and :code:`ovjan` is the Jupyter Hub username.
Both values are extraced from the Jupyter Lab interface and need no further configuration.



Manual Agent
^^^^^^^^^^^^

Can be configured as an account using :code:`display-name`,:code:`provider-url` and :code:`user-id`


.. code-block::

	"agent": {
	       "display-name": "John Snow",
	       "provider-url": "accounts.my_lrs.com/agents",
	       "user-id": "john_snow_1",
	    }

or as an mail-account :code:`display-name` and :code:`email`.

.. code-block::

	"agent": {
	       "display-name": "John Snow",
	       "email": "john.snow@snowmail.com"
	    }


This setting results in a custom, manually configurated xApi Agent.

.. code-block:: json
	
	{
	  "objectType": "Agent",
	  "account": {
	    "name": "john_snow_1",
	    "homePage": "accounts.my_lrs.com/agents"
	  },
	  "name":"John Snow" 
	}

respectively

.. code-block:: json
	
	{
	  "objectType": "Agent",
	  "mbox": "john.snow@snowmail.com",
	  "name":"John Snow" 
	}


.. _juxl-configuration-lrs:

lrs
***


Configures the Learning Record Store (LRS).

If configured, all statements are forwarded to the lrs.

  
A minmal configuration contains only an URL:

.. code-block::

	"lrs": {
	  "url":"https://lrs.myDomain.com/data/xAPI"
	}

Authentication can be configured in three different ways:


Using a Token
^^^^^^^^^^^^^

.. code-block::

	"lrs":{
	  "url":"https://lrs.myDomain.com/data/xAPI",
	  "authentication":{
	    "token":"Basic ABCDEFGHIJKMYAUTHORIZATIONVALUE1234"
	  }
	}

The :code:`token` value will be directly used as an authorization header for the comminciation to the lrs. 

Using a Key-Secret pair
^^^^^^^^^^^^^^^^^^^^^^^


.. code-block::

	"lrs": {
	  "url":"https://lrs.myDomain.com/data/xAPI",
	  "authentication":{
	    "key":"1a2b3c4d5e6d1a2b3c4d5e6d1a2b3c4d5e6d",
	    "secret":"11aa22bb33cc44dd55ee66ff77gg"
	  }
	}

A token is generated using the key and secret and will be directly used as an authorization header for the comminciation to the lrs.


Using an Python evnrionment variable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


.. code-block::

	"lrs":{
	  "url":"https://lrs.myDomain.com/data/xAPI",
	  "authentication":{
	    "python-environment":"MY_TOKEN_VAR"
	   }
	}

Juxl will evaluate the environment variable :code:`MY_TOKEN_VAR` and use the value as  an authorization header for the comminciation to the lrs.

**This is especially usefull for an approach with** :ref:`Juxl Hub Auth <juxl-hub-auth>` **.**

:ref:`Juxl Hub Auth <juxl-hub-auth>` requires an JupyterHub authentication token.
The JupyterHub authentication token is stored inside the :code:`JUPYTERHUB_API_TOKEN` environment variable.
So setting 

.. code-block::

	"lrs":{
	 "url":"https://lrs.myDomain.com/data/xAPI",
	 "authentication":{
	   "python-environment":"JUPYTERHUB_API_TOKEN",
	  }
	}
	
will automatically use the :code:`JUPYTERHUB_API_TOKEN` for authentication with :ref:`Juxl Hub Auth <juxl-hub-auth>`.
The authentication token does not need to be set manually!



It is also possible to use another environment variable when communication to another LRS, etc. 


disabledJuxlExtensions
**********************

A list of Plugin-IDs of Juxl Extension.

.. code-block::

	"disabledJuxlExtensions": [
		"@juxl/logging",
		"@juxl/performance-extension:srtt",
		"@juxl/juxl-extension:settingsUI"
	    ],
	    
All Listed Extensions are not connected to the Juxl Interface.
Instead they are each connected to an independently running Juxl instance.
If an :ref:`LRS <juxl-configuration-lrs>` is configured, statements are not forwarded to the lrs.

All other functionallity apart from logging to the global Juxl Interface are still available to the plugins.
To disable Plugins completley, use Jupyter Lab to disable the extension see `JupyterLab enabling-disabling extensions <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html?highlight=disable#enabling-and-disabling-extensions>`_



platform
********

Configure platform context information of xApi Statements.

.. code-block::

	"platform":{
	  "id":"juypter.rwth-aachen.de",
	  "name": "JupyterLab"
	}

The configuration results in an xApi Context:


.. code-block:: json


	{
	  "platform": "JuypterLab",
	  "contextActivities": {
	    "category": [
	      {
	        "objectType":"Activity",
	        "id": "jupyter.rwth-aachen.de",
	        "definition": "..."
	      }
	    ]
	  }
	}
	

Upon default the platform name :code:`JupyterLab` is used.
The default platform id is the url of the JupyterLab instance.




.. _juxl-configuration-pseudonym:

pseudonym
*********

This option should not be set by the user.
The pseudonym is gernerated automatically if the agent option :code:`pseudo-anonymous` is set. 


.. code-block::

	"pseudonym":"anonymous-a1b2d3c4"


It can be removed to create a new pseudonym at any time.




