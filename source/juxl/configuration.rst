.. _juxl-configuration:





Configuration
#############


Juxl can be configurated using the **Juxl Settings UI**
avaiable under the header :code:`Settings -> Juxl Settings`


.. image:: juxl_settings_ui.png
   :width: 1000%
   :align: center


or the **advanced settings editor**
avaiable under the header :code:`Settings -> Advanced Settings Editor -> Juxl Settings`
 
 
.. image:: user_settings.png
   :width: 1000%
   :align: center


The Juxl Settings UI will edit the advanced settings editor.
Both ways of chaning the juxl settings are equivalent, but the advanced settings editor provides more options.
To change the default settings see :ref:`deploy default settings <juxl-deploy-default-settings>`.

In the following sections we discuss each configuration option in detail and afterwards focus on how to configure Juxl with Juxl Hub Auth. 

.. toctree::
   :maxdepth: 1
   
   ./configuration/all_options
   ./configuration/juxl_with_juxl_hub_auth


