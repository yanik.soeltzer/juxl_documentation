.. _juxl-installation:

Installation
************
Juxl and Juxl-extensions can be installed as any other JupyterLab extension by either using the extension manager or the Jupyter labextension command line interface. 


Using Extension Manager:
^^^^^^^^^^^^^^^^^^^^^^^^

You can use the Extension Manager Ui to install Juxl and the mentioned Juxl-extensions.

.. warning::
    You need to enable the use of extensions. Read how to enable JupyterLab extensions at `Using the Extension Manager <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html#managing-extensions-using-the-extension-manager>`_.

    This can results in some security concerns.
    See the official disclaimer at `JupyerLab Documentation <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html#managing-extensions-using-the-extension-manager>`_.


Afterwards search for :code:`@juxl/juxl-extension` and click on install.

More infromation about the Jupyter Extension Manager are available `here <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html#managing-extensions-using-the-extension-manager>`_.



Using jupyter labextension:
^^^^^^^^^^^^^^^^^^^^^^^^^^^


Run

.. code-block:: bash

	root@device:~$ jupyter labextension install @juxl/juxl-extension

to install Juxl using the Jupyter labextension cli.


Run

.. code-block:: bash

	root@device:~$ jupyter labextension install @juxl/juxl-extension @juxl/logging @juxl/log-console
	
For a Juxl installation with a log-console and extensive logging.
	

More information about The Jupyter labextension cli `here <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html#managing-extensions-with-jupyter-labextension>`_.




.. seealso::
	
	`JupyterLab Extension Documentation <https://jupyterlab.readthedocs.io/en/stable/user/extensions.html>`_
	  Detailed documentation on JupyterLab Extensions. Includes installation, managing and listing extensions.

