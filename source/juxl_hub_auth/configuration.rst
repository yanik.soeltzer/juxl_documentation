.. _juxl-hub-auth-configuration:

Configuration
=============

Juxl-Hub-Auth is completely configurable using environmental variables.



.. _juxl-hub-auth-required-configuration:

Requried Configuration
**********************

If the :code:`HTTP_LIBRARY=dummy` option is **not** set, the following configurations are required: 
Juxl-Hub-Auth will not start, if either one is not set.

``JUPYTER_HUB_URL``
-------------------

The Url of the JupyterHub instance used to authenticate the users.

example values:

* ``http://localhost:8080/hub``
* ``https://jupyter.rwth-aachen.de/hub``



``XAPI_LRS_URL``
----------------
The Url of the Lrs instance that stores the supmitted statements.

example values:

- ``http://localhost:8080/data/xAPI``
- ``http://my.lrs.com/data/xAPI``
- ``https://lrs.elearn.rwth-aachen.de/data/xAPI``



``XAPI_LRS_AUTH_TOKEN``
-----------------------

A token used to authenticate requests against the LRS.

example values:

- ``Basic NjNlNzkzNmE5N2Q5MTc1OTMxMTM5NDE2MzlhMN2NkODVlYTIxMDE0MGFjMmRiNTllOGI5NWI4``
- ``Token ae2b409c6e864087b7ede7e0a038c4c2``

.. _juxl-hub-auth-optional-configuration:

Optional Configuration
**********************

These configuration options are optional and typically have default-values.


``CERTFILE``
------------

Used for ssh-authentication. 
A path to a ``.crt`` file containing the ssh-certificate.
If a ``CERTFILE`` is set, the ``KEYFILE`` should also be set.

example values:

- ``./ssl/localhost.crt``
- ``/etc/letsencrypt/live/my-juxl-hub.com/fullchain.crt``


``KEYFILE``
-----------

Used for ssh-authentication.
A path to a ``.crt`` file containing the ssh-certificate key.

example values:

- ``./ssl/localhost.key``
- ``/etc/letsencrypt/live/my-juxl-hub.com/privkey.key``


``HTTP_LIBRARY``
----------------

Configures which Python http library is used to communicate to the Lrs and JupyterHub instances.

can be any of:

- ``urllib``
- ``requests``
- ``dummy``
- ``httplib2``

If the ``dummy`` library is used, no statements are forwarded and all users are accepted without being authenticated.
This makes configuring ``JUPYTER_HUB_URL``, ``XAPI_LRS_URL`` and ``XAPI_LRS_AUTH_TOKEN`` pointless.

The default value is ``httplib2``.


``CACHE_USERNAME``
------------------

Weather or not Usernames should be cached.
If a user is authenticated using a JupyterHub token, the username belonging to the token is cached in a redis database.
If the same token is used again, no request to JupyterHub is send, instead the cached username is used.

can be any of:

- ``True``
- ``False``


If ``CACHE_USERNAME=True`` a redis database is required, since Juxl-Hub-Auth will use a Redis database for caching the toke-username entries.

The default value is ``True``.


``REDIS_HOST``
--------------

Only used if ``CACHE_USERNAME=True``.
Sets the hostname of the Redis database.

The default value is ``redis``.



``REDIS_PORT``
--------------

Only used if ``CACHE_USERNAME=True``.
Sets the port of the Redis database.

The default value is ``6379``.


``LOG_LEVEL``
-------------

How verbose Juxl-Hub-Auth should log.

can be any of:

- ``CRITICAL``
- ``ERROR``
- ``WARNING``
- ``INFO``
- ``DEBUG``

The default value is ``WARNING``.

