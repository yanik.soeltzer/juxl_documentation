.. _juxl-hub-auth-installation:

Installation
============




Minimal Setup
*************

To run a dummy Juxl-Hub-Auth using docker-compose create a docker-compose file containing

.. literalinclude :: code_files/docker-compose-minimal.yml
   :language: yaml

and run :code:`docker-compose up`

This should output: 

.. code-block:: bash

	[2021-07-23 13:12:17 +0000] [1] [INFO] Starting gunicorn 20.1.0
	[2021-07-23 13:12:17 +0000] [1] [INFO] Listening at: http://0.0.0.0:443 (1)
	[2021-07-23 13:12:17 +0000] [1] [INFO] Using worker: sync
	[2021-07-23 13:12:17 +0000] [8] [INFO] Booting worker with pid: 8
	INFO:root:set LogLevel to DEBUG
	INFO:root:Use DummyJupyterHub Interface
	WARNING:root:Using a Dummy JupyterHub! No real authentication provided.
	INFO:root:Use DummyLrs Interface
	WARNING:root:Using a Dummy LRS! No statements are saved.


Because we set the :code:`HTTP_LIBRARY=dummy` option, we do not need to configure any JupyterHub or LRS jet.
The Juxl-Hub-Auth accepts all request and dismisses all received statements.

To ensure the Juxl-Hub-Auth is running and accessible, visit http://127.0.0.1:3333.
The website should repond with :code:`Everything OK :D`.


Now you can configure Juxl to send statements to the Juxl-Hub-Auth. 
To do that set the lrs-url in the juxl-config. For more information see the :ref:`Juxl Configuration <juxl-configuration>` documentation.


.. code-block:: json

	{
	  "lrs": {
	    "url":"http://127.0.0.1:3333"
	  }
	}

Afterwards, we will see a short description of the inserted statements in the Juxl-Hub-Auth log


.. code-block:: bash

	DEBUG:root:insert statement 'anonymous accessed JupyterLab'
	DEBUG:root:insert statement 'anonymous opened Jupyter Notebook'



Advanced Setup
**************

A more zadvanced configuration could look like the following.

.. literalinclude :: code_files/docker-compose-extended.yml
   :language: yaml



The following sections describe how to configure a JupyterHub instance for authentication, how to configure a LRS for statement-forwarding and other configurations.
