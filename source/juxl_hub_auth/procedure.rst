
#######################
Juxl Hub Auth Procedure
#######################



For every request such as inserting a statement, Juxl Hub Auth executes the following procedure:

.. image:: visualizations/responsibilities.jpg
   :width: 1000%
   :alt: Juxl-Hub-Responsibilites in a Sequence Diagram
   :align: center



We will discuss each step in the following sections

.. toctree::
   :maxdepth: 1
   :numbered:
   
   ./procedure/authenticating_jupyter_hub_user
   ./procedure/modifying_statements
   ./procedure/forwarding_statements
   

