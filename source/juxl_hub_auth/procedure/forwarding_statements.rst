.. _juxl_hub_auth forwarding_statemetns:

Forwarding Statements
=====================



.. image:: visualizations/responsibilities_3.jpg
   :width: 1000%
   :alt: Juxl-Hub-Responsibilites in a Sequence Diagram
   :align: center


Finally, Juxl-Hub-Auth forwards the modified statement to the LMS.

It expects the LMS to return the statement-id of the inserted statement, which will then be forwarded to Juxl.

If the LMS returns some html error code, Juxl-Hub-Auth will analyze the error and respont to Juxl with a error.


