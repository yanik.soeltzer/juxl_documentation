
Developing Juxl Hub Auth
========================

After cloning the repository, 
setting environment variables 
and installing the required python libraries
the ``development_server.py`` provides an easy way to start a hot-reloading flask debug server.   

.. code-block:: bash

	root@device:~/juxl_hub_auth$ export $(cat .env.example)
	root@device:~/juxl_hub_auth$ python3 -m pip install -r requirements.txt
	root@device:~/juxl_hub_auth$ python3 development_server.py

