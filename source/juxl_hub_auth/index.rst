.. _juxl-hub-auth:

Juxl Hub Auth
#############


.. note::
	
	Make sure To read the :ref:`getting-started-components` first to get an overview about how the different components work together.



Juxl Hub Auth is a Jupyterlab xApi Logging Interface Jupyter Hub Authentication Reverse Proxy.
It simulates a LRS.
The difference from the Juxl point of view, is that a typical LRS requires an LRS Authentication Token, while the Juxl-Hub-Auth requires a JuypterHub token to authenticate requests.

.. image:: visualizations/lrs_difference.jpg
   :width: 1000%
   :align: center


.. _juxl-hub-auth-why-juxl-hub-auth:

Why Juxl Hub Auth ?
*******************


Juxl Hub Auth serves to increase security in an environment with multiple user.

Without Juxl Hub Auth, every juxl instance would have access to an LRS Authentication key.
Those keys are restricted in their access.
Depending on the lrs, a key can be restricted to e.g. only read statements.
Such restrictions are typically broad.
Custom restrictions such as a user can only insert statements where he is the actor are not possible.





