
.. _settings-composition:

Settings Composition
********************

Jupyterlab generates the settings for extensions using three layers:

1. user-settings
2. overrides.json
3. Extension default settings


Where a higher layer overrides the lower layer.
The user-settings override all layer.
The following paragraphs explain each settings layer in detail using the settings composition of `@juxl/juxl`. 


Extension default settings
^^^^^^^^^^^^^^^^^^^^^^^^^^

The Extension default settings are provided by the extension.
They are delivered alongside the Javascript and Typescript code, typically in a folder named *schema*.
It is possible to provide multipe settings files per extension.
The only settings property provided by `@juxl/juxl` is the agent type, resulting in this default configuration:

.. code-block:: json

	{
	  "agent": "anonymous"
	}
	
.. _overrides.json:

Overrides.json
^^^^^^^^^^^^^^

The `override.json` file is a special file containing configurations for **all** JupyterLab extensions.
The `override.json` is under the `JupyterLab Application Settings Directory <https://jupyterlab.readthedocs.io/en/stable/user/directories.html#application-settings-directory>`_, which itself is under the `JupyterLab Application Directory <https://jupyterlab.readthedocs.io/en/stable/user/directories.html#application-directory>`_.

Examples for this directory are:

* :code:`/opt/conda/share/jupyter/lab/settings`
* :code:`/miniconda3/envs/jl/share/jupyter/lab/settings`

depending on the installation.
If we would want to override the agent type to use pseudo-anonymization, we create the following file: 

.. code-block:: json

	{
	  "@juxl/juxl-extension:juxl": {
	    "agent": "pseudo-anonymous"
	  }
	}


.. note::
	The structure of the overrides.json differs from the extension default settings and user-settings!
	Settings are encapsulated inside a `"@extension:settings"` property.



User-Settings
^^^^^^^^^^^^^

The user settings should only be edited throug the `advanced settings editor`.
They are saved under the jupyterlab config directory. 
Run :code:`jupyter --config` to get the config directory.

If the user generates a pseudonym it is saved as a user-settings resulting in such a file:


.. code-block:: json

	{
	  "agent": "pseudo-anonymous",
	  "pseudonym": "anonymous-z880b3j1c"
	}
    

