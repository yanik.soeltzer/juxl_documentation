

What is Juxl?
*************

Juxl is an xApi Logging interface for `JupyterLab <https://jupyterlab.readthedocs.io/>`_.
It is used to log user interactions using the `xApi <https://xapi.com/overview/>`_ format to a database in most cases a Learning Record Store (LRS).

An interaction that can be logged is the edit process of a Jupyter Notebook cell.
The process would look like the following:


.. image:: what_is_juxl.png
   :width: 1000%
   :align: center



#. User edits cell 
#. Juxl caputres username, cell-id, execution time, etc
#. Juxl creates an xAPI statement
#. Juxl sends the xAPI statement to a Learning Record Store 

Depending on what Juxl-Extensions are installed, different user activites can be captured.
The logged data can then be used in further analysis.


The name Juxl can refer to the Juxl-Ecosystem or the JupyterLab-Juxl extension depending on the context. The exact annotation and differences are explained in :ref:`getting-started-components`.

