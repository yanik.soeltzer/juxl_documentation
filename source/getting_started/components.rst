.. _getting-started-components:

Component overview
******************

The Project-Juxl consist of multiple Software components.
This overview lists the most important components and provides a short description for each.



.. image:: component_overview.jpg
   :width: 1000%
   :align: center




JupyterLab-Juxl (Juxl)
^^^^^^^^^^^^^^^^^^^^^^

The JupyterLab-Juxl, in short Juxl and also reffered to by its package name ``@juxl/juxl-extension`` is the fundamental building block for Project-Juxl.
JupyterLab-Juxl provides a logging interface for all Juxl Extensions.
It can be configured to foward statements to a LRS, authenticate statements using the JupyterHub user and enable or disable Juxl-Extensions.
JupyterLab-Juxl does not log any activities by its own except for changes in its configuration.
It relies on Juxl-Extension to use the provided logging interface to log e.g. changes to a notebook cell.
Read more about JupyterLab :ref:`Juxl <juxl>`.


.. _gettings-started-juxl-extensions:

Juxl Extensions 
^^^^^^^^^^^^^^^

Juxl Extensions are JupyterLab extensions that extends the JupyterLab-Juxl.
Most extensions log specific user iteractions.
Noteworthy extensions are:

* **Juxl Basic Logging**

  Juxl Basic Logging logs most Notebook interactions such as opening and closing a notebook, executing, editing, moving cells and much more user interactions.

* **Juxl Log Console**

  The Juxl Log Console provides a panel to view logged statements.
  This is especially usefull for seeing which statements are logged, debugging Juxl-Extension.

* **Juxl Performance Measuring**

  This extensions measures startup performances of Juxl-Extensions as well as metrics on the logging-capabilities of Juxl.





Juxl JupyterHub Authentication Proxy 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Juxl JupyterHub Authentication Proxy (Juxl-Hub-Auth) is required for secure authentication using JupyterHub.
Using this reverse proxy, no LRS auth key needs to be stored in the user space and no custom authentication service needs to be deployed.
Further details are shown in :ref:`juxl-hub-auth`.









